# scrollspy.js
Un snippet simple et léger qui met automatiquement la classe *active* au lien de navigation de la section actuellement consultée ([démonstration ici](https://raphpion.gitlab.io/scrollspy/)).
## Installation
 1. Télécharger le fichier [scrollspy.js](https://gitlab.com/raphpion/scrollspy/-/blob/master/scrollspy.js) dans le dossier de votre projet, ou copier-coller le contenu et l'intégrer directement dans votre HTML.
 2. Le script doit être appelé ou intégré dans le body après vos balises nav et section.
## Prérequis
 - Vos sections doivent être placées dans le même ordre que vos liens de navigation.
 - Vos sections doivent être définies comme des balises *section*.
 - Vos liens de navigation doivent être les seuls (ou premiers) liens de votre balise *nav*. 