//* scrollspy.js
// Ajout de la classe 'active' au lien du menu de navigation correspondant à la fonction en cours de visionnement
// © Copyright Raphaël Pion 2020 ─ tous droits réservés

const sections = document.querySelectorAll('section')
const navLinks = document.querySelectorAll('nav a')

scrollspy()
window.onscroll = scrollspy

function scrollspy() {
  for (let i = 0; i < sections.length - 1; i++) {
    window.pageYOffset >= sections[i].offsetTop && window.pageYOffset < sections[i + 1].offsetTop
      ? navLinks[i].classList.add('active')
      : navLinks[i].classList.remove('active')
  }
  window.pageYOffset >= sections[sections.length - 1].offsetTop
    ? navLinks[sections.length - 1].classList.add('active')
    : navLinks[sections.length - 1].classList.remove('active')
}
